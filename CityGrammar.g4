grammar CityGrammar;

programm :
      lib*
      main*
      ;

//number oprations
// ali **= (mmd + 12 * 7 ** 4 + ++javad);
down_numberic_opration : (PLUS | MINES | PERCENT | DEVIDE);
shift_oprations : (LANGEL LANGEL | RANGEL RANGEL);
bit_op : SINGLE_AND | SINGLE_OR | '^';
number : PLUS INTEGER | MINES INTEGER | INTEGER;
number_op : exprtion;
exprtion: bit_function (bit_op bit_function)* ;
bit_function :underline (UNDERLINE underline)*;
underline : shift (shift_oprations shift)*;
shift : add_sub (SHARP add_sub)*;
add_sub :mult (down_numberic_opration mult)* ;
mult :  pow (STAR pow)* ;
pow :  semi_number (STAR STAR semi_number)*;
semi_number:  number | inc_dec | VAR_NAME | LPAREN exprtion RPAREN ;

float_number : PLUS (INTEGER |INTEGER '.' INTEGER) | MINES (INTEGER|INTEGER '.' INTEGER) | (INTEGER|INTEGER '.' INTEGER);
float_op : float_exprtion;
float_exprtion: bit_functio_float (bit_op bit_functio_float)*;
bit_functio_float : float_underline (UNDERLINE float_underline)*;
float_underline : float_shift (shift_oprations float_shift)*;
float_shift : float_add_sub (SHARP float_add_sub)*;
float_add_sub:  float_mult (down_numberic_opration float_mult)* ;
float_mult:  float_pow (STAR float_pow)* ;
float_pow :  float_semi_number (STAR STAR float_semi_number)*;
float_semi_number:  float_number | sientictic_number |  inc_dec | VAR_NAME | LPAREN float_exprtion RPAREN ;

sientictic_number : float_number  number_exprtion;
number_exprtion : 'e' ((PLUS | MINES ) INTEGER);

// increment decrement
inc_dec : increment | decrement;
decrement : (VAR_NAME MINMIN | MINMIN VAR_NAME);
increment : (VAR_NAME PLPL | PLPL VAR_NAME) ;
PLPL : PLUS PLUS;
MINMIN : MINES MINES;

// Operators
SINGLE_AND : '&';
AND : SINGLE_AND SINGLE_AND;
SINGLE_OR : '|';
OR : SINGLE_OR SINGLE_OR;
STAR : '*';
NOT : '!';
PLUS: '+';
MINES : '-';
UNDERLINE : '_';
SHARP : '#';
SLASH : '/';
DEVIDE : SLASH SLASH;
PERCENT : '%';
EQUAL: '==';
NOTEQUAL: '!=';
ASSIGN: '=' ;


IF: 'if';
SEMICOLON: ';';
LPAREN: '(';
RPAREN: ')';
LANGEL  : '<';
RANGEL : '>';
LACOLAD   : '{';
RACOLAD  : '}';
COLON : ':';

FOR : 'for';
SWITCH : 'switch';
FUNCTION : 'Function';
CASE : 'case';
WHILE : 'while';
ENDIF: 'endif';
PRINT: 'print';
INT: 'int';
FLOAT: 'float';
CONSTTANT : 'constants' ;
IMPORT: 'import';
CLASS : 'class';
TRUE : 'true';
FALSE : 'false';
NULL : 'null';

VAR_NAME : ([a-z] | [A-Z]) AB_NAME INTEGER*;
INTEGER: [0-9][0-9]*;

lib : LIB ;
LIB : IMPORT LANGEL LIB_NAME RANGEL SEMICOLON ;
LIB_NAME : NAME;

main : LACOLAD classes RACOLAD ;
classes : one_class+ ;
one_class : (CLASS_DEFINATION class_body_define);
CLASS_DEFINATION : CLASS LANGEL NAME RANGEL COLON ;
class_body_define : LANGEL class_Body RANGEL ;
class_Body : statements+ ;
statements : var_define | function;

//function
function : function_define function_body_define;
function_define : function_name LPAREN (function_input | RPAREN);
function_input : declaration ',' function_input | end_input ;
end_input: declaration RPAREN;
function_name: FUNCTION VAR_NAME;
function_body_define : LANGEL function_body RANGEL;
function_body : (var_define | if_statement | loop_for | loop_while | switch_case)*;

//condition
conditions : constants_exprtions;
//condition | condition AND conditions | (LPAREN conditions RPAREN) |condition OR conditions | (LPAREN conditions RPAREN);
//condition : condition_side condition_oprators condition_side;
//condition_side :(INTEGER | TRUE | FALSE | NULL | VAR_NAME | inc_dec);
//condition_oprators : (EQUAL | NOTEQUAL | LANGEL | (LANGEL ASSIGN) | RANGEL | (RANGEL ASSIGN));

//if
if_statement : IF LPAREN conditions RPAREN LACOLAD if_body RACOLAD;
if_body : function_body;

//loop for
loop_for : FOR LPAREN for_triple RPAREN LACOLAD loop_body RACOLAD;
for_triple : (first_for_state | SEMICOLON) (seconde_for_state | SEMICOLON)  (thierd_for_state | );
first_for_state : var_define ;
seconde_for_state : conditions SEMICOLON;
thierd_for_state : inc_dec ;
loop_body : function_body;

//loop while
loop_while : WHILE LPAREN conditions RPAREN LACOLAD while_body RACOLAD;
while_body : function_body;

//switch case
switch_case : SWITCH LPAREN VAR_NAME RPAREN COLON LACOLAD cases RACOLAD;
cases : r_case+;
r_case : CASE LPAREN INTEGER RPAREN COLON (LACOLAD case_body RACOLAD | (var_define | if_statement | loop_for | loop_while) | );
case_body : function_body;

//varible
var_define :((declaration_normal assignment | declaration_normal) |
            (declaration_numberic_int assignment_numberic_int | declaration_numberic_int) |
            (declaration_numberic_float assignment_numberic_float | declaration_numberic_float))
             SEMICOLON;
declaration: (declaration_numberic_float |declaration_numberic_int | declaration_normal);
//numberic
assignment_numberic_int : assignment_oprator number_op ;
assignment_numberic_float : assignment_oprator float_op ;
declaration_numberic_int :(INT | FLOAT) VAR_NAME;
declaration_numberic_float :FLOAT VAR_NAME;


//constats
assignment : ASSIGN constants_exprtions;
declaration_normal : CONSTTANT VAR_NAME ;
constants_exprtions : first_step (first_CONSTANT_OPRATION first_step)* | (float_op | number_op) first_CONSTANT_OPRATION_EXTENDS (float_op | number_op);
first_step : temp_branch (seconde_CONSTANT_OPRATION temp_branch)*;
temp_branch : normal_constants | LPAREN constants_exprtions RPAREN;
normal_constants : (FALSE | TRUE | NULL);
first_CONSTANT_OPRATION : EQUAL | NOTEQUAL | RANGEL ;
first_CONSTANT_OPRATION_EXTENDS : first_CONSTANT_OPRATION | LANGEL |RANGEL ASSIGN | LANGEL ASSIGN;
seconde_CONSTANT_OPRATION : 'and' | 'not' | 'or';


assignment_oprator :ASSIGN | (PLUS ASSIGN) | (MINES ASSIGN) | (STAR ASSIGN) | (SLASH ASSIGN) |
                   (STAR STAR ASSIGN) | (PERCENT ASSIGN) | (DEVIDE ASSIGN) ;

// Variable names
AB_NAME: ([a-z] | [A-Z])+;
NAME: AB_NAME INTEGER*;

// Ignore all white spaces and comment
WS: [ \t\r\n]+ -> skip ;
COMMENT : '//' COMMENT_LINE* -> skip ;
COMMENT_LINE : ~'\n';

